using ClassLibraryCore.ClassLibrary;
using System;
using Xunit;

namespace TestsUsingXUnit
{
    public class UnitTest1
    {
        private Calculator calculator = new Calculator();

        [Fact]
        public void Add_ReturnsCorrectFromAddedParameters()
        {
            //arrange
            int expectedValue = 10;

            //act
            int returnedValue = this.calculator.Add(5, 5);

            //assert
            Assert.Equal(expectedValue, returnedValue);
        }

        [Fact]
        public void Add_ReturnsWrongFromAddedParameters()
        {
            //arrange
            int expectedValue = 10;

            //act
            int returnedValue = this.calculator.Add(5, 2);

            //assert
            Assert.NotEqual(expectedValue, returnedValue);
        }

        [Fact]
        public void Subtract_ReturnsCorrectFromAddedParameters()
        {
            //arrange
            int expectedValue = 10;

            //act
            int returnedValue = this.calculator.Subtract(11, 1);

            //assert
            Assert.Equal(expectedValue, returnedValue);
        }

        [Fact]
        public void Subtract_ReturnsWrongFromAddedParameters()
        {
            //arrange
            int expectedValue = 10;

            //act
            int returnedValue = this.calculator.Subtract(11, 2);

            //assert
            Assert.NotEqual(expectedValue, returnedValue);
        }

        [Fact]
        public void Multiply_ReturnsCorrectFromAddedParameters()
        {
            //arrange
            int expectedValue = 10;

            //act
            int returnedValue = this.calculator.Multiply(5, 2);

            //assert
            Assert.Equal(expectedValue, returnedValue);
        }

        [Fact]
        public void Multiply_ReturnsWrongFromAddedParameters()
        {
            //arrange
            int expectedValue = 10;

            //act
            int returnedValue = this.calculator.Multiply(4, 2);

            //assert
            Assert.NotEqual(expectedValue, returnedValue);
        }

        [Fact]
        public void Delete_ReturnsCorrectFromAddedParameters()
        {
            //arrange
            int expectedValue = 0;

            //act
            int returnedValue = this.calculator.Delete();

            //assert
            Assert.Equal(expectedValue, returnedValue);
        }
    }
}
