using System;
using Xunit;
using ClassLibraryCore.ClassLibrary;

namespace mytestingxunit
{
    public class CalculatorTest
    {

        private Calculator calculator = new Calculator();

        [Fact]
        public void Add_ReturnsCorrectFromAddedParameters()
        {
            //arrange
            int expectedValue = 10;

            //act
            int returnedValue = this.calculator.Add(5, 5);

            //assert
            Assert.Equal(expectedValue, returnedValue);
        }

        [Fact]
        public void Add_ReturnsWrongFromAddedParameters()
        {
            //arrange
            int expectedValue = 10;

            //act
            int returnedValue = this.calculator.Add(5, 2);

            //assert
            Assert.NotEqual(expectedValue, returnedValue);
        }
    }
}
