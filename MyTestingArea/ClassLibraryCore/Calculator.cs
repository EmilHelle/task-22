﻿using System;

namespace ClassLibraryCore
{
    namespace ClassLibrary
    {
        public class Calculator
        {

            public int Add(int firstNumber, int secondNumber)
            {
                return firstNumber + secondNumber;
            }
            public int Subtract(int firstNumber, int secondNumber)
            {
                return firstNumber - secondNumber;
            }
            public int Multiply(int firstNumber, int secondNumber)
            {
                return firstNumber * secondNumber;
            }
            public int Delete()
            {
                return 0;
            }
        }
    }

}
