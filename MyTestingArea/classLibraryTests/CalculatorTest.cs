﻿using ClassLibraryFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace classLibraryTests
{
    public class CalculatorTest
    {
        private Calculator calculator = new Calculator();

        [Fact]
        public void Add_ReturnsCorrectFromAddedParameters()
        {
            //arrange
            int expectedValue = 10;

            //act
            int returnedValue = this.calculator.Add(5, 5);

            //assert
            Assert.Equal(expectedValue, returnedValue);
        }

        [Fact]
        public void Add_ReturnsWrongFromAddedParameters()
        {
            //arrange
            int expectedValue = 10;

            //act
            int returnedValue = this.calculator.Add(5, 2);

            //assert
            Assert.NotEqual(expectedValue, returnedValue);
        }

        [Fact]
        public void Subtract_ReturnsCorrectFromAddedParameters()
        {
            //arrange
            int expectedValue = 10;

            //act
            int returnedValue = this.calculator.Subtract(100, 10);

            //assert
            Assert.Equal(expectedValue, returnedValue);
        }

        [Fact]
        public void Subtract_ReturnsWrongFromAddedParameters()
        {
            //arrange
            int expectedValue = 10;

            //act
            int returnedValue = this.calculator.Subtract(100, 10);

            //assert
            Assert.NotEqual(expectedValue, returnedValue);
        }

        [Fact]
        public void Multiply_ReturnsCorrectFromAddedParameters()
        {
            //arrange
            int expectedValue = 10;

            //act
            int returnedValue = this.calculator.Multiply(5, 2);

            //assert
            Assert.Equal(expectedValue, returnedValue);
        }

        [Fact]
        public void Multiply_ReturnsWrongFromAddedParameters()
        {
            //arrange
            int expectedValue = 10;

            //act
            int returnedValue = this.calculator.Multiply(5, 2);

            //assert
            Assert.NotEqual(expectedValue, returnedValue);
        }
    }
}
