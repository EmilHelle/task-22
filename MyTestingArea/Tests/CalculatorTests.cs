﻿using ClassLibrary;
using Xunit;

namespace Tests
{
    public class CalculatorTests
    {
        private Calculator calculator = new Calculator();

        [Fact]
        public void Add_ReturnsCorrectAddedParameters()
        {
            //arrange
            int expectedValue = 10;

            //act
            int returnedValue = this.calculator.Add(5, 5);

            //assert
            Assert.Equal(expectedValue, returnedValue);
        }
    }
}
