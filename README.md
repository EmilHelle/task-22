Task 22


I initially created classLibrary to run tests, it did not work, then used a Xunit project
to handle the tests and it then worked. I tried again to revert to classLibrary and then when
I tried to run the tests again and they showed up in test explorer but are marked as 'not run'.
I decided to keep the Xunit project so that the tests work, but also have the classLibrary version
to show that it is there and can be used later if we are going to build further on the project.

-ClassLibraryCore was needed so Xunit project could access the calculator class
-ClassLibraryFramework was needed so classLibraryTests could access the calculator class

- The tests work